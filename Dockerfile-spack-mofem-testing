FROM       likask/spack_mofem_dependencies:latest
MAINTAINER Lukasz.Kaczmarczyk@glasgow.ac.uk

ADD mofem /mofem

ENV MOFEM_SRC_DIR=/mofem
ENV MOFEM_ROOT_DIR=/mofem_build
ENV MOFEM_INSTALL_DIR=/mofem_build/um
ENV MOFEM_BUILD_DIR=/mofem_build/lib
ENV OMPI_MCA_btl_vader_single_copy_mechanism=none

RUN       mkdir -p /spack_views \
          && cd /spack_views \
	  && spack view symlink -i cmake cmake

# install mofem core library
RUN       mkdir -p $MOFEM_BUILD_DIR \
          && cd $MOFEM_BUILD_DIR \
          && spack setup mofem-cephas@develop ^openblas~avx2 \
          && ./spconfig.py \
            -DMOFEM_BUILD_TESTS=YES \
            -DMPI_RUN_FLAGS="--allow-run-as-root" \
            $MOFEM_SRC_DIR \
          && make -k -j$(nproc) install; /bin/true \
          && /spack_views/cmake/bin/ctest -D Experimental; /bin/true \
          && make clean

# create installation view
RUN       mkdir -p $MOFEM_INSTALL_DIR \
          && cd $MOFEM_INSTALL_DIR \ 
          && spack view symlink -i um_view mofem-cephas@develop ^openblas~avx2

# install users modules
RUN       mkdir -p $MOFEM_INSTALL_DIR/build \
          && cd $MOFEM_INSTALL_DIR/build \
          && spack setup mofem-users-modules@develop ^openblas~avx2 \
          && ./spconfig.py \
            -DFM_VERSION_MAJOR=0 -DFM_VERSION_MINOR=0 -DFM_VERSION_BUILD=0 \
            -DWITH_METAIO=YES \
            -DMOFEM_UM_BUILD_TESTS=YES \
            -DMPI_RUN_FLAGS="--allow-run-as-root" \
            $MOFEM_SRC_DIR/users_modules \
          && make -k -j$(nproc) install; /bin/true \
          && /spack_views/cmake/bin/ctest -VV -D Experimental; /bin/true \
          && make clean
