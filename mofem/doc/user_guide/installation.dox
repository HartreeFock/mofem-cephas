/*! \page installation Installation

\tableofcontents

We prefer to use the scripts provided below to install user version or
developer version of MoFEM with [Spack](https://spack.io) as a package manager.

\section installation_user_version User version

User version of MoFEM installation provides only binary files (not the source
codes) that allow users 
to run analyses in MoFEM straightaway. You may wish follow these steps for
installation that will include binary (executable) files of *Basic User Module* and *Fracture Module*

- Locate the directory where you would like to install MoFEM

- Download installation script to that directory [`install_mofem_user.sh`](scripts/install_mofem_user.sh)

- Change mode of the script (if neccesary)
  ~~~~~~
  chmod +x install_mofem_user.sh
  ~~~~~~

- Run the script (sudo privileges may be required)
  ~~~~~~
  ./install_mofem_user.sh
  ~~~~~~ 

- Test run `elasticity` program
  ~~~~~~
  cd YOUR_INSTALLATION_DIRECTORY
  cd mofem_install/um_view/elasticity
  ./elasticity -my_file LShape.h5m -my_order 2
  ~~~~~~ 

\section installation_developer_version Developer version

Developer version of MoFEM installation provides you with highest flexibility
with all the source codes.
You can experiment the codes, create your own module, and always stay
up-to-date to the latest developement of MoFEM. You may wish to follow these
steps for installation that will include source code and executables of *Core
Library*, *Basic User Module*, and *Fracture Module* with both release and
debug build types

- Locate the directory where you would like to install MoFEM

- Download installation script to that directory [`install_mofem_developer.sh`](scripts/install_mofem_developer.sh)

- Change mode of the script (if neccesary)
  ~~~~~~
  chmod +x install_mofem_developer.sh
  ~~~~~~

- Run the script (sudo privileges may be required)
  ~~~~~~
  ./install_mofem_developer.sh
  ~~~~~~ 

- Test run `elasticity` program
  ~~~~~~
  cd YOUR_INSTALLATION_DIRECTORY
  cd mofem_install/um/build_release/basic_finite_elements/elasticity
  ./elasticity -my_file LShape.h5m -my_order 2
  ~~~~~~ 

\subsection installation_developer_version_add_new_module Compile codes and add new module in MoFEM

For developer, you may wish to have a look at these two tutorials to compile
codes and add new module in MoFEM

- \ref how_to_compile_program

- \ref how_to_add_new_module_and_program

\note If you have issues installing MoFEM, please let us know on [MoFEM
Q&A](https://groups.google.com/forum/#!categories/mofem-group). Enjoy MoFEM!


\subsection installation_reference Other documents

We do recommend you to use the scripts above for installation; however, if you
would like to read about different ways to install MoFEM, you can have a look here

- Spack and Docker

  - \subpage install_spack

  - \subpage install_docker

- Ubuntu and macOS (reference only, no longer maintained and supported)

  - \subpage install_ubuntu

  - \subpage install_macosx

*/
