MoFEM v0.9.2

- New logging interface based on boost.log
- Fixes in PrismInterface
- Developments of mesh cutting algorithm
- Interface for side volume elements for contact prism elements
- H-div for contact elements
- Surface pressure ALE
- Contact element ALE
- Gauss point convection for contact (moderate slip)
- ALM frictionless contact
- Computation of the real contact area and active set of gauss points
- Rotational Dirichlet BCs
- 8 new lessons
- New tutorials
- Code refactoring and minor fixes
